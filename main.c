#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

void intercala(int *v, int inicio, int meio, int fim) {

    int j, k;
    int fim1 = 0, fim2 = 0;
    int tamanho = (fim - inicio) + 1;
    int p1 = inicio;
    int p2 = (meio + 1);
    int *temp = (int *) malloc(tamanho * sizeof(int));

    if (temp != NULL) {
        for (int i = 0; i < tamanho; i++) {
            if (!fim1 && !fim2) {
                if (v[p1] < v[p2])
                    temp[i] = v[p1++];
                else
                    temp[i] = v[p2++];
                if (p1 > meio) fim1 = 1;
                if (p2 > fim) fim2 = 1;
            } else {
                if (!fim1)
                    temp[i] = v[p1++];
                else
                    temp[i] = v[p2++];
            }
        }

        for (j = 0, k = inicio; j < tamanho; j++, k++) v[k] = temp[j];
    }

    free(temp);
}

// ideal para grandes
void mergeSort(int *v, int inicio, int fim) {
    if (inicio < fim) {
        int meio = (inicio + fim) / 2;
        mergeSort(v, inicio, meio);
        mergeSort(v, meio + 1, fim);
        intercala(v, inicio, meio, fim);
    }
}

void imprimirVetorDesordenado(int *v, int n) {
    printf("\nVetor n�o ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

void imprirmirVetorOrdenado(int *v, int n) {

    mergeSort(v, 0, (n - 1));
    printf("\nVetor ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

int main() {
    setlocale(LC_ALL, "Portuguese");

    int vetor1[6] = {2, 1, 4, 3, 6, 5};
    imprimirVetorDesordenado(vetor1, 6);
    imprirmirVetorOrdenado(vetor1, 6);

    int vetor2[10] = {10, 9, -8, 7, -6, 5, -4, 3, 2, 11};
    imprimirVetorDesordenado(vetor2, 10);
    imprirmirVetorOrdenado(vetor2, 10);

    // inicializa um vetor com 50 posi��es. Valor default � 0.
    int vetor3[50] = {};

    // preenche o vetor3 de 50 posi��es com n�meros aleat�rios.
    for (int i = 0; i < 50; i++) vetor3[i] = rand();

    imprimirVetorDesordenado(vetor3, 50);
    imprirmirVetorOrdenado(vetor3, 50);

    return 0;
}
